package com.placco.data.booking;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import com.placco.data.common.Address;
import com.placco.data.common.Document;
import com.placco.data.common.Reviews;
import com.placco.data.enums.BookingStatus;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "booking")
public class Booking {

	@Id
	private String id;

	@Indexed(unique = true)
	private String bookingId;

	private String customerId;

	private Address bookingAddress;

	private String assignedProfessionalId;

	private Date bookingDate;

	private BookingStatus bookingStatus;

	private Date startTimeSlot;

	private Date endTimeSlot;

	private Date actualStartTimeSlot;

	private Date actualEndTimeSlot;

	private List<Document> pictures;

	private String cancellationReason;

	private Reviews customerReview;

	private Reviews professionalReview;

	private Map<BookingStatus, Date> statusChangedTime;

	private String description;

	private String createdBy;

	private String updatedBy;

	private Date createdDate;

	private Date updatedDate;
}
