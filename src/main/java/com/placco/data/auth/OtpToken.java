package com.placco.data.auth;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.placco.data.enums.OtpMode;

import lombok.Data;

@Data
@Document(collection = "otp_session")
public class OtpToken {

	@Id
	private String id;

	@Indexed
	private String sessionId;

	private String mobileOrEmailId;

	private String otp;

	private OtpMode otpMode;

	private boolean isVerified;

	private boolean isUsed;

	private Date createdDate;

	private Date updatedDate;

	private Date verfiedDate;
}
