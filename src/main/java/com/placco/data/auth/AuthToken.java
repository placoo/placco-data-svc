package com.placco.data.auth;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "auth")
public class AuthToken {

	@Id
	private String id;

	private String userId;

	private long expiry;

	private Date lastUsedDate;

}
