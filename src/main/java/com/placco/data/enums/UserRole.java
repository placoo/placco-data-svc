package com.placco.data.enums;

public enum UserRole {
	Super_Admin("Admin"), Hr_Admin("Human Resource"), Bd_Admin("Business Development"), Finance_Admin("Finance"),
	Operation_Admin("Operations");

	private UserRole(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}
}
