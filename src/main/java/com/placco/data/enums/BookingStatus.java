package com.placco.data.enums;

public enum BookingStatus {
	Pending, InProgress, Completed, Rejected, Cancelled;
}
