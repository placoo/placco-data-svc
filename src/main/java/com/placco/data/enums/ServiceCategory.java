package com.placco.data.enums;

public enum ServiceCategory {
	basic, cleaning, saloon, appliances;
}
