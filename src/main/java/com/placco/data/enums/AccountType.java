package com.placco.data.enums;

public enum AccountType {
	saving("Saving"), current("Current");

	private AccountType(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}
}
