package com.placco.data.enums;

public enum DocType {
	avatar("User Avatar"), address("Address Proof"), gst("GST Document"), pan("Pan Card"), bank("Bank Document"),
	aadhaar("Aadhaar"), others("Others");

	private DocType(String desc) {
		this.desc = desc;
	}

	private String desc;

	public String getDesc() {
		return desc;
	}

}
