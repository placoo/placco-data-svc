package com.placco.data.enums;

public enum ExpenseCategory {
	Employee_Compensation("Employee Compensation"), Professional_Conmpensation("Professional Conmpensation"),
	Professional_Inventory("Professional Inventory"), Office_Inventory("Office Inventory"),
	Marketing_Inventory("Marketing Inventory"), Marketing_Charges("Marketing Charges"),
	Office_Rent_Maintenance("Office Rent/Maintenance"),
	Professional_Fuel_Reimbursement(" Professional/Fuel Reimbursement"),
	Employee_Reimbursement("Employee Reimbursement"), Other_Reimbursement("Other Reimbursement"),
	Spare_Parts_Reimbursement("Spare parts Reimbursement"), Gst_Paid("GST Paid"), Income_Tax("Income Tax"),
	Amount_Paid_Credit_Basis("Amount Paid Credit Basis"), Employee_Incentives("Employee Incentives"),
	Professional_Incentives("Professional Incentives"), Freelancing_Compensation("Freelancing Compensation"),
	Thrid_Party_Expenses("Thrid Party Expenses"), Director_Salary("Director Salary");

	private String desc;

	private ExpenseCategory(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
