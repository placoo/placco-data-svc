package com.placco.data.enums;

public enum PictureType {
	Main, Thumbnail;
}
