package com.placco.data.employee;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;

import com.placco.data.common.Address;
import com.placco.data.common.Document;
import com.placco.data.enums.UserRole;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "employee")
public class Employee {

	@Id
	private String id;

	private String employeeId;

	private String password;

	@NotBlank(message = "Employee first name cannot be empty")
	private String firstName;

	private String lastName;

	@NotBlank(message = "Employee mobile number cannot be empty")
	private String mobileNumber;

	private String alternateMobileNumber;

	@NotBlank(message = "Employee emailId cannot be empty")
	private String emailId;

	@NotNull(message = "Date of joining  cannot be empty")
	private Date dateOfJoining;

	@NotNull(message = "Date of birth  cannot be empty")
	private Date dateOfBirth;

	@NotNull(message = "Date of joining  cannot be empty")
	private UserRole role;

	@NotBlank(message = "Designation of joining  cannot be empty")
	private String designation;

	private Address address;

	private List<EmployeeEducationDetails> educationDetails;

	private List<EmployeeExperienceDetails> employeeExperienceDetails;

	private List<Document> employeeDocumentDetails;

	private EmployeeBankDetails employeeBankDetails;

	private Document avatar;

	private boolean active;

	private String createdBy;

	private String updatedBy;

	private Date createdDate;

	private Date updatedDate;

}
