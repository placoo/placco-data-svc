package com.placco.data.employee;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.placco.data.common.Document;
import com.placco.data.enums.AccountType;

import lombok.Data;

@Data
public class EmployeeBankDetails {

	@NotBlank(message = "Account holder name cannot be empty")
	private String accountHolderName;

	@NotBlank(message = "Account number cannot be empty")
	private String accountNumber;

	@NotBlank(message = "Confirm account number cannot be empty")
	private String confirmAccountNumber;

	@NotBlank(message = "ifsc cannot be empty")
	private String ifsc;

	@NotNull(message = "Account Type cannot be empty")
	private AccountType accountType;

	private Document cancelCheque;

}
