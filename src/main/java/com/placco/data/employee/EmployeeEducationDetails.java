package com.placco.data.employee;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class EmployeeEducationDetails {

	private String id;

	@NotBlank(message = "Course cannot be empty")
	private String course;

	@NotBlank(message = "Institute cannot be empty")
	private String institute;

	@NotBlank(message = "Start year cannot be empty")
	private String startYear;

	@NotBlank(message = "End year cannot be empty")
	private String endYear;

}
