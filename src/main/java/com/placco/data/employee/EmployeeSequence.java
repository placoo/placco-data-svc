package com.placco.data.employee;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "employee_sequence")
public class EmployeeSequence {

	@Id
	private String id;

	private int nextSequence;
}
