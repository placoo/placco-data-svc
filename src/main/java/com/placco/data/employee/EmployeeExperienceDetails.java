package com.placco.data.employee;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class EmployeeExperienceDetails {

	private String id;

	@NotBlank(message = "Company name cannot be empty")
	private String companyName;

	@NotBlank(message = "Sesignation cannot be empty")
	private String designation;

	@NotBlank(message = "Start year cannot be empty")
	private String startYear;

	@NotBlank(message = "End year cannot be empty")
	private String endYear;

}
