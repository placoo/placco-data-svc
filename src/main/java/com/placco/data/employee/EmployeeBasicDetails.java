package com.placco.data.employee;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.placco.data.enums.UserRole;

import lombok.Data;

@Data
public class EmployeeBasicDetails {

	@NotBlank(message = "Employee first name cannot be empty")
	private String firstName;

	private String lastName;

	@NotBlank(message = "Employee mobile number cannot be empty")
	private String mobileNumber;

	private String alternateMobileNumber;

	@NotBlank(message = "Employee emailId cannot be empty")
	@Email(message = "Invalid email Id")
	private String emailId;

	@NotNull(message = "Date of joining  cannot be empty")
	private Date dateOfJoining;

	@NotNull(message = "Date of birth  cannot be empty")
	private Date dateOfBirth;

	@NotNull(message = "Date of joining  cannot be empty")
	private UserRole role;

	@NotBlank(message = "Designation of joining  cannot be empty")
	private String designation;

}
