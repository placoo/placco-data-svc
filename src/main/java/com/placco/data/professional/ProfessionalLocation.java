package com.placco.data.professional;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "employee_location")
public class ProfessionalLocation {

	@Id
	private String id;

	private String professionalId;

	private double[] position;

}
