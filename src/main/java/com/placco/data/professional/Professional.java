package com.placco.data.professional;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.placco.data.common.Address;
import com.placco.data.common.Document;
import com.placco.data.enums.ServiceCategory;
import com.placco.data.enums.ServiceSubCategory;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "professional")
public class Professional {

	@Id
	private String id;

	private String firstName;

	private String lastName;

	private String password;

	private String mobileNumber;

	private String alternateMobileNumber;

	private String emailId;

	private ServiceCategory serviceCategory;

	private List<ServiceSubCategory> serviceSubCategories;

	private List<Document> documents;

	private Address address;

	private Document avatar;

	private boolean isAvailable;

	private Date lastAvailableDate;

	private Date activeDate;

	private boolean active;

	private String createdBy;

	private String updatedBy;

	private Date createdDate;

	private Date updatedDate;

}
