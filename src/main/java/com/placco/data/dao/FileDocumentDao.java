package com.placco.data.dao;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.placco.data.document.FileDocument;

@Repository
public class FileDocumentDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public FileDocument saveOrUpdate(final FileDocument fileDocument) {
		Date now = new Date();
		if (fileDocument.getId() == null) {
			fileDocument.setId(new ObjectId().toString());
			fileDocument.setCreatedDate(now);
		}
		fileDocument.setUpdatedDate(now);
		return mongoTemplate.save(fileDocument);
	}

	public FileDocument findById(final String id) {
		return mongoTemplate.findById(id, FileDocument.class);
	}

	public FileDocument findUploadedDocById(final String id) {
		FileDocument fileDocument = mongoTemplate.findById(id, FileDocument.class);
		if (fileDocument.isUploaded()) {
			return fileDocument;
		}
		return null;
	}
}
