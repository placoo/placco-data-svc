package com.placco.data.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.placco.data.auth.AuthToken;

@Repository
public class AuthDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public AuthToken saveOrUpdate(final AuthToken token) {
		return mongoTemplate.save(token);
	}

	public AuthToken findById(final String id) {
		return mongoTemplate.findById(id, AuthToken.class);
	}

	public void delete(final String id) {
		mongoTemplate.remove(new Query(where("_id").is(id)), AuthToken.class);
	}
}
