package com.placco.data.dao;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.placco.data.employee.Employee;
import com.placco.data.employee.EmployeeSequence;

@Repository
public class EmployeeDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	private int pageLimit = 100;

	public int getNextEmployeeSequence() {
		Query query = new Query();
		Criteria criteria = Criteria.where("_id").is("employee_Sequence");
		query.addCriteria(criteria);
		EmployeeSequence counter = mongoTemplate.findAndModify(query, new Update().inc("seq", 1),
				new FindAndModifyOptions().returnNew(true).upsert(true), EmployeeSequence.class);
		return !Objects.isNull(counter) ? counter.getNextSequence() : 1;
	}

	public Employee loginByEmail(final String emailId, String passwd) {
		Query query = new Query();
		Criteria criteria = Criteria.where("emailId").is(emailId).and("password").is(passwd);
		query.addCriteria(criteria);
		Employee employee = mongoTemplate.findOne(query, Employee.class);
		return employee != null && employee.isActive() ? employee : null;
	}

	public Employee loginByMobileNumber(final String mobileNumber, String passwd) {
		Query query = new Query();
		Criteria criteria = Criteria.where("mobileNumber").is(mobileNumber).and("password").is(passwd);
		query.addCriteria(criteria);
		Employee employee = mongoTemplate.findOne(query, Employee.class);
		return employee != null && employee.isActive() ? employee : null;
	}

	public Employee getByMobileNumber(final String mobileNumber) {
		Query query = new Query();
		Criteria criteria = Criteria.where("mobileNumber").is(mobileNumber);
		query.addCriteria(criteria);
		Employee employee = mongoTemplate.findOne(query, Employee.class);
		return employee != null && employee.isActive() ? employee : null;
	}

	public Employee getByEmailId(final String emailId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("emailId").is(emailId);
		query.addCriteria(criteria);
		Employee employee = mongoTemplate.findOne(query, Employee.class);
		return employee != null && employee.isActive() ? employee : null;
	}

	public boolean isEmailExist(final String emailId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("emailId").is(emailId);
		query.addCriteria(criteria);
		Employee employee = mongoTemplate.findOne(query, Employee.class);
		return employee != null;
	}

	public boolean isMobileNumberExist(final String mobileNumber) {
		Query query = new Query();
		Criteria criteria = Criteria.where("mobileNumber").is(mobileNumber);
		query.addCriteria(criteria);
		Employee employee = mongoTemplate.findOne(query, Employee.class);
		return employee != null;
	}

	public Employee saveOrUpdate(final Employee employee) {
		Date now = new Date();
		if (employee.getId() == null) {
			employee.setId(new ObjectId().toString());
			employee.setCreatedDate(now);
		}
		employee.setUpdatedDate(now);
		return mongoTemplate.save(employee);
	}

	public Employee findById(final String id) {
		Employee employee = mongoTemplate.findById(id, Employee.class);
		if (employee.isActive()) {
			return employee;
		}
		return null;
	}

	public void delete(final String id) {
		Employee employee = mongoTemplate.findById(id, Employee.class);
		employee.setActive(false);
		saveOrUpdate(employee);
	}

	public List<Employee> search(final String name, final String employeeId, final String emailId,
			final Long startIndex, final Long pageSize) {
		Query query = new Query();
		int limit = pageSize == null ? pageLimit : pageSize.intValue();
		int skip = startIndex == null ? 0 : startIndex.intValue() - 1;
		query.limit(limit);
		query.skip(skip);
		Criteria criteria = getCriteria(name, employeeId, emailId);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, Employee.class);
	}

	public long searchCount(final String name, final String employeeId, final String emailId) {
		Query query = new Query();
		Criteria criteria = getCriteria(name, employeeId, emailId);
		query.addCriteria(criteria);
		return mongoTemplate.count(query, Employee.class);
	}

	private Criteria getCriteria(final String name, final String employeeId, final String emailId) {
		Criteria criteria = null;
		if (StringUtils.isNotBlank(name)) {
			criteria = Criteria.where("firstName").regex(name);
		}
		if (StringUtils.isNotBlank(emailId)) {
			if (criteria == null) {
				criteria = Criteria.where("emailId").regex(emailId);
			} else {
				criteria.and("emailId").regex(emailId);
			}
		}
		if (StringUtils.isNotBlank(employeeId)) {
			if (criteria == null) {
				criteria = Criteria.where("employeeId").regex(employeeId);
			} else {
				criteria.and("employeeId").regex(employeeId);
			}
		}
		return criteria == null ? new Criteria() : criteria;
	}
}
