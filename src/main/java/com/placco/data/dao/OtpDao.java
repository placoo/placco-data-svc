package com.placco.data.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.placco.data.auth.OtpToken;

@Repository
public class OtpDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public OtpToken saveOrUpdate(final OtpToken token) {
		Date now = new Date();
		if (token.getId() == null) {
			token.setId(new ObjectId().toString());
			token.setCreatedDate(now);
		}
		token.setUpdatedDate(now);
		return mongoTemplate.save(token);
	}

	public OtpToken findById(final String id) {
		return mongoTemplate.findById(id, OtpToken.class);
	}

	public OtpToken findBySessionId(final String sessionId) {
		return mongoTemplate.findOne(new Query(where("sessionId").is(sessionId)), OtpToken.class);
	}
}
