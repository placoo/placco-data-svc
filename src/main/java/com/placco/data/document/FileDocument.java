package com.placco.data.document;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.placco.data.enums.DocType;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "file_document")
public class FileDocument {

	@Id
	private String id;

	private String fileName;

	private String s3Url;

	private DocType docType;

	private String number;

	private String nameOfDocument;

	private boolean uploaded;

	private String createdBy;

	private String updatedBy;

	private Date createdDate;

	private Date updatedDate;

}
