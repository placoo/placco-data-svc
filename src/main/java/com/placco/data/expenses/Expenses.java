package com.placco.data.expenses;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.placco.data.common.Document;
import com.placco.data.enums.ExpenseCategory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Expenses {

	private String id;

	@NotNull(message = "Expense Category cannot be empty")
	private ExpenseCategory expenseCategory;

	private String description;

	private String fileDescription;

	@Min(value = 100, message = "Cost should be greater than 1 rs.")
	private long cost;

	private Document document;

	private Date createdDate;

	private Date updatedDate;

}
