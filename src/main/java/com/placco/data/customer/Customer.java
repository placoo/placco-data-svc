package com.placco.data.customer;

import java.util.Date;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
@org.springframework.data.mongodb.core.mapping.Document(collection = "professional")
public class Customer {

	@Id
	private String id;

	private String firstName;

	private String lastName;

	private String password;

	private String mobileNumber;

	private String alternateMobileNumber;

	private String emailId;

	private boolean active;

	private String createdBy;

	private String updatedBy;

	private Date createdDate;

	private Date updatedDate;
}
