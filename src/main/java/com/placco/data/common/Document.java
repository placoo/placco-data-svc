package com.placco.data.common;

import javax.validation.constraints.NotBlank;

import com.placco.data.enums.DocType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Document {

	@NotBlank(message = "id  cannot be empty")
	private String id;

	private String fileName;

	private String docUrl;

	private DocType docType;

	private String number;

	private String nameOfDocument;
}
