package com.placco.data.common;

import java.util.Map;

import com.placco.data.enums.NotificationTemplate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SnsNotification {

	private NotificationTemplate notificationTemplate;

	private Map<String, String> dynamicContent;

	private String sendTo;

	private String subject;

	private String body;

	private String[] cc;

	private String[] bcc;

}
