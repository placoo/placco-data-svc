package com.placco.data.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Pagination {

	private long startIndex;

	private long pageNumber;
}
