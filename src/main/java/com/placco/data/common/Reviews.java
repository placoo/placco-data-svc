package com.placco.data.common;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Reviews {

	private String userId;

	private String userName;

	private String userAvatar;

	private Double rating;

	private String review;

	private Date createdDate;
}
