package com.placco.data.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Rating {

	private long totalRating;

	private double averageRating;
}
