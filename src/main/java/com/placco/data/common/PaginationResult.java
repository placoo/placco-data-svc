package com.placco.data.common;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PaginationResult<T> {

	private long total;

	private List<T> data;
}
