package com.placco.data.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {

	private String id;

	private String addressLine;

	private String landmark;

	private String city;

	private String pincode;

	private String state;

}
