package com.placco.data.common;

import com.placco.data.enums.PictureType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Picture {

	private String id;

	private String url;

	private PictureType pictureType;

	private String metaData;
}
